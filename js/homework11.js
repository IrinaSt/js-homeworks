let listCount = Number(prompt('Введите количество пунктов в списке'));
let listContentArray = [];
for (let i = 1; i <= listCount; i++) {
	let listContent = prompt(`Введите содержимое ${i} пункта`);
	listContentArray.push(listContent);
}
let list = document.createElement('ul');
let content = listContentArray.map(function (element) {

	return `<li>${element}</li>`
});

list.innerHTML = content;

document.body.appendChild(list);

setTimeout(function () { document.body.removeChild(list); }, 10000);