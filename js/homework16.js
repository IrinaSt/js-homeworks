let changeThemeBtn = document.getElementById('changeTheme');

let localStorageCheck = () => {
	let existTheme = localStorage.getItem('theme');
	if (existTheme) {
		document.body.style.backgroundColor = 'black';
	} else {
		document.body.style.backgroundColor = '#fff';
	}
}

let changeThemeColor = () => {
	let existTheme = localStorage.getItem('theme');
	if (existTheme) {
		localStorage.removeItem('theme');

	} else {
		localStorage.setItem('theme', 'dark');
	}

	localStorageCheck();


}
changeThemeBtn.addEventListener('click', changeThemeColor, false);
