let milisec = 0;
let sec = 0;
let min = 0;
let milisecValue = document.getElementById('miliseconds');
let secValue = document.getElementById('seconds');
let minValue = document.getElementById('minutes');
let startBtn = document.getElementById('startTimer');
let pauseBtn = document.getElementById('pauseTimer');
let endBtn = document.getElementById('endTimer');

let timer = () => {

	let startedTimer = setInterval(function () {
		milisec++;
		milisecValue.innerHTML = milisec;

		if (milisec == 999) {
			milisec = 0;
			sec++;
		}
		secValue.innerHTML = sec;

		if (sec == 59) {
			sec = 0;
			min++;
		}
		minValue.innerHTML = min;
	},
		1);

	let pauseTimer = () => {
		clearInterval(startedTimer);;
	};
	pauseBtn.addEventListener('click', pauseTimer, false);
};

let deleteTime = () => {
	milisec = 0;
	sec = 0;
	min = 0;
	milisecValue.innerHTML = milisec;
	secValue.innerHTML = sec;
	minValue.innerHTML = min;
}

startBtn.addEventListener('click', timer, false);

endBtn.addEventListener('click', deleteTime, false);
