let checkNumberIntValue = (someValue) => {

	while (someValue == null || Number.isInteger(Number(someValue)) === false || Number(someValue) <= 1) {
		if (someValue == null) {
			someValue = prompt("Введите число повторно");
			continue;
		}
		if (Number.isInteger(Number(someValue) === false)) {
			someValue = prompt("Введите целое число!");
			continue;
		} else {
			someValue = prompt("Введите число больше 1");
		}
	}
	return Number(someValue);
};

let simpleNumbers = () => {
	let userNumber = checkNumberIntValue(prompt('Введите число'));
	for (let i = 1; i <= userNumber; i++) {
		console.log(i);
	}
};

//2.1 task

let twoSimpleNumbers = () => {
	let m = checkNumberIntValue(prompt('Введите первое число'));
	let n = checkNumberIntValue(prompt('Введите второе число'));

	while (m >= n) {
		alert('Введите оба числа заново');
		m = checkNumberIntValue(prompt('Введите первое число'));
		n = checkNumberIntValue(prompt('Введите второе число'));
	}
	for (let i = m; i <= n; i++) {
		console.log(i);
	}
};

document.getElementById('secondTask').addEventListener('click', simpleNumbers, false);
document.getElementById('secondTaskNn').addEventListener('click', twoSimpleNumbers, false);
