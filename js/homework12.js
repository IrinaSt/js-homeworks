let btnAction = document.getElementById('actionBtn');


let drawCircle = () => {
	document.body.removeChild(btnAction);
	let inputColor = document.createElement('input');
	inputColor.value = 'Введите цвет!';

	let inputDiameter = document.createElement('input');
	inputDiameter.value = 'Введите диаметр!';

	let drawButton = document.createElement('button');
	drawButton.innerHTML = 'Нарисовать круг!';
	document.body.appendChild(inputDiameter);
	document.body.appendChild(inputColor);
	document.body.appendChild(drawButton);

	drawButton.addEventListener('click', function () {
		let diameter = inputDiameter.value + 'px';
		let color = inputColor.value;
		let circle = document.createElement('div');
		document.body.appendChild(circle);
		circle.style.width = diameter;
		circle.style.height = diameter;
		circle.style.borderRadius = '50%';
		circle.style.backgroundColor = color;
		circle.style.marginTop = '50px';
	});

};

btnAction.addEventListener('click', drawCircle, false);


