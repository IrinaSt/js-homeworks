let factorial = (n) => {
	return (n != 1) ? n * factorial(n - 1) : 1;
}

let countFactorial = () => {
	let userNumber = checkNumberValue(prompt('Введите число'));
	alert(factorial(userNumber));
};

document.getElementById('thirdTask').addEventListener('click', countFactorial, false);
