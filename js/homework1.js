let checkNumberValue = (someValue) => {

	while (someValue == null || isNaN(Number(someValue)) || Number(someValue) <= 0) {
		if (someValue == null) {
			someValue = prompt("Введите число повторно");
			continue;
		}
		if (isNaN(Number(someValue))) {
			someValue = prompt("Введите число!");
			continue;
		} else {
			someValue = prompt("Введите число больше 0");
		}
	}
	return Number(someValue);
};


let checkStringValue = (someValue) => {
	while (someValue == null || !isNaN(Number(someValue))) {
		someValue = prompt("Ожидается строка!");
		continue;
	}
	return someValue;
}


let modalWindows = () => {

	let userName = checkStringValue(prompt("Назови имя"));
	let userAge = checkNumberValue(prompt("Твой возраст"));
	let welcomeMsg = () => {
		alert('Welcome ' + userName);
	}

	if (userAge < 18) {
		alert('You are not allowed to visit this website');
	}
	if (userAge >= 18 && userAge <= 22) {
		let userAnswer = confirm('Are you sure you want to continue');
		if (userAnswer) {
			welcomeMsg();
		}
		else {
			alert('You are not allowed to visit this website');
		}
	}

	if (userAge > 22) {
		welcomeMsg();
	}
};
document.getElementById('firstTask').addEventListener('click', modalWindows, false);

