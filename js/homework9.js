let userDates = () => {

	let today = new Date();
	let todayYear = today.getFullYear();
	let userDate = prompt("Введите дату рождения в формате dd.mm.yyyy");
	let userYear = Number(userDate.slice(6));
	let userMonth = Number(userDate.slice(3, 5));
	let userDay = Number(userDate.slice(0, 2));
	let zodiak, chineseAnimal;
	let userAge = todayYear - userYear;
	let chineseNumber = (userYear - 3) % 12;

	if ((userDay >= 22 && userMonth === 12) || (userDay <= 19 && userMonth === 1)) {
		zodiak = "Козерог";
	}

	if ((userDay >= 22 && userMonth === 1) || (userDay <= 18 && userMonth === 2)) {
		zodiak = "Водолей";
	}

	if ((userDay >= 19 && userMonth === 2) || (userDay <= 20 && userMonth === 3)) {
		zodiak = "Рыбы";
	}
	if ((userDay >= 21 && userMonth === 3) || (userDay <= 19 && userMonth === 4)) {
		zodiak = "Овен";
	}

	if ((userDay >= 20 && userMonth === 4) || (userDay <= 20 && userMonth === 5)) {
		zodiak = "Телец";
	}

	if ((userDay >= 21 && userMonth === 5) || (userDay <= 20 && userMonth === 6)) {
		zodiak = "Близнецы";
	}
	if ((userDay >= 21 && userMonth === 6) || (userDay <= 22 && userMonth === 7)) {
		zodiak = "Рак";
	}

	if ((userDay >= 23 && userMonth === 7) || (userDay <= 22 && userMonth === 8)) {
		zodiak = "Лев";
	}

	if ((userDay >= 23 && userMonth === 8) || (userDay <= 22 && userMonth === 9)) {
		zodiak = "Дева";
	}

	if ((userDay >= 23 && userMonth === 9) || (userDay <= 22 && userMonth === 10)) {
		zodiak = "Весы";
	}
	if ((userDay >= 23 && userMonth === 10) || (userDay <= 21 && userMonth === 11)) {
		zodiak = "Скорпион";
	}
	if ((userDay >= 22 && userMonth === 11) || (userDay <= 21 && userMonth === 12)) {
		zodiak = "Стрелец";
	}

	switch (chineseNumber) {
		case 0: chineseAnimal = "Свинья";
			break;
		case 1: chineseAnimal = "Мышь";
			break;
		case 2: chineseAnimal = "Корова";
			break;
		case 3: chineseAnimal = "Тигр";
			break;
		case 4: chineseAnimal = "Заяц";
			break;
		case 5: chineseAnimal = "Дракон";
			break;
		case 6: chineseAnimal = "Змея";
			break;
		case 7: chineseAnimal = "Лошадь";
			break;
		case 8: chineseAnimal = "Овца";
			break;
		case 9: chineseAnimal = "Обезьяна";
			break;
		case 10: chineseAnimal = "Петух";
			break;
		case 11: chineseAnimal = "Собака";
			break;
	}

	alert("Вам " + userAge + ' лет');
	alert("Вы по знаку зодиака  " + zodiak + ' и по восточному гороскопу ' + chineseAnimal);
};

document.getElementById('ninthTask').addEventListener('click', userDates, false);